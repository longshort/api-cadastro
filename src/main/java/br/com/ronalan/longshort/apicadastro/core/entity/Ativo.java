package br.com.ronalan.longshort.apicadastro.core.entity;

import br.com.ronalan.longshort.base.entity.BaseEntity;

/**
 * @author ronaldo.lanhellas
 * Responsavel por manter os ativos/papeis do nosso  dominio
 * exemplo: PETR4, PETR3, ITUB4 ...
 * */
public class Ativo extends BaseEntity {
	
	private String codigo;
	private String descricao;
	
	//TODO: colocar dataHoraCriacao como protected
	
	public Ativo() {
	}
	
	public Ativo(String id) {
		this.id = id;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
