package br.com.ronalan.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.ronalan.longshort.apicadastro.core.entity.Ativo;
import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.ronalan.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.ronalan.longshort.base.dto.response.ListaErroEnum;
import br.com.ronalan.longshort.base.dto.response.ResponseDataErro;
import br.com.ronalan.longshort.base.gateway.SalvarGateway;

/**
 * Cadastra o ativo
 * 
 * @author ronaldo.lanhellas
 */
@Service
public class AtualizarAtivoUseCaseImpl extends BaseAtivoUseCase implements AtualizarAtivoUseCase {

	private final SalvarGateway<Ativo> salvarGateway;
	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

	public AtualizarAtivoUseCaseImpl(SalvarGateway<Ativo> salvarGateway,
			BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
	}

	/**
	 * atualiza o cadastro do ativo
	 * 
	 * @param input
	 * @return {@link AtivoResponse}
	 **/
	@Override
	public AtivoResponse executar(AtivoRequest input) {
		logger.info("Iniciando atualização do ativo de código " + input.getCodigo() + " para descrição "
				+ input.getDescricao());

		AtivoResponse response = new AtivoResponse();
		response.setCodigo(input.getCodigo());
		response.setDescricao(input.getDescricao());

		validarCamposObrigatorios(input, response);
		if (response.getResponse().getErros().size() > 0) {
			return response;
		}

		Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(input.getCodigo());

		if (opAtivo.isPresent()) {
			Ativo ativo = opAtivo.get();
			ativo.setDescricao(input.getDescricao());
			salvarGateway.salvar(ativo);
			response.setId(ativo.getId());
		} else {
			String msg = "Ativo não encontrado com código " + input.getCodigo();
			logger.error(msg);
			response.getResponse().adicionarErro(new ResponseDataErro(msg, ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
		}

		return response;

	}

}
