package br.com.ronalan.longshort.apicadastro.core.usecase;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.ronalan.longshort.base.usecase.BaseUseCase;

/**
 * lista de ativos paginados
 * 
 * @author ronaldo.lanhellas
 */
public interface BuscarAtivosUseCase extends BaseUseCase<Integer, PaginadoAtivoResponse> {
}
