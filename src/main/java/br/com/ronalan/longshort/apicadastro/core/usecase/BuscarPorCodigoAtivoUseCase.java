package br.com.ronalan.longshort.apicadastro.core.usecase;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.ronalan.longshort.base.usecase.BaseUseCase;

/**
 * busca pelo código do ativo
 * 
 * @author ronaldo.lanhellas
 */
public interface BuscarPorCodigoAtivoUseCase 
extends BaseUseCase<String, AtivoResponse> {
}
