package br.com.ronalan.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.ronalan.longshort.apicadastro.core.entity.Ativo;
import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.ronalan.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.ronalan.longshort.base.dto.response.ListaErroEnum;
import br.com.ronalan.longshort.base.dto.response.ResponseDataErro;
import io.micrometer.core.instrument.util.StringUtils;

/**
 * busca pelo código do ativo
 * 
 * @author ronaldo.lanhellas
 */
@Service
public class BuscarPorCodigoAtivoUseCaseImpl implements BuscarPorCodigoAtivoUseCase {

	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	private Logger log = LoggerFactory.getLogger(BuscarPorCodigoAtivoUseCaseImpl.class);

	public BuscarPorCodigoAtivoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
	}

	/**
	 * executa a busca pelo código do ativo
	 * 
	 * @param codigo
	 * @return {@link AtivoResponse}
	 **/
	@Override
	public AtivoResponse executar(String codigo) {
		log.info("Buscando ativo pelo código " + codigo);
		AtivoResponse response = new AtivoResponse();
		response.setCodigo(codigo);

		validarCamposObrigatorios(codigo, response);
		if (!response.getResponse().getErros().isEmpty()) {
			return response;
		}

		Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(codigo);

		if (opAtivo.isPresent()) {
			log.debug("Ativo encontrado com código " + codigo);
			response.setDescricao(opAtivo.get().getDescricao());
			response.setId(opAtivo.get().getId());
		} else {
			log.debug("Ativo não encontrado com código " + codigo);
			response.getResponse().adicionarErro(new ResponseDataErro("Ativo não encontrado com código " + codigo,
					ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
		}

		return response;
	}

	private void validarCamposObrigatorios(String codigo, AtivoResponse response) {
		log.debug("Validando campos obrigatórios");
		log.debug("codigo = " + codigo);
		if (StringUtils.isBlank(codigo)) {
			response.getResponse().adicionarErro(
					new ResponseDataErro("Campo código é obrigatório", ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}

	}
}
