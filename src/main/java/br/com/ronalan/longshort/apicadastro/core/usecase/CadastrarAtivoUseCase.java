package br.com.ronalan.longshort.apicadastro.core.usecase;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.ronalan.longshort.base.usecase.BaseUseCase;

/**
 * Cadastra o ativo
 * 
 * @author ronaldo.lanhellas
 */
public interface CadastrarAtivoUseCase extends BaseUseCase<AtivoRequest, AtivoResponse> {
}
