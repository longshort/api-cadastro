package br.com.ronalan.longshort.apicadastro.core.usecase;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.CorrelacaoAtivoRequest;
import br.com.ronalan.longshort.apicadastro.core.usecase.dto.CorrelacaoAtivoResponse;
import br.com.ronalan.longshort.base.usecase.BaseUseCase;

public interface CadastrarCorrelacaoAtivoUseCase 
	extends BaseUseCase<CorrelacaoAtivoRequest, CorrelacaoAtivoResponse> {
}
