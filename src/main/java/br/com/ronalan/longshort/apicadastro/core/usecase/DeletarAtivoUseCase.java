package br.com.ronalan.longshort.apicadastro.core.usecase;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.ronalan.longshort.base.usecase.BaseUseCase;

/**
 * deleta o ativo
 * 
 * @author ronaldo.lanhellas
 */
public interface DeletarAtivoUseCase extends BaseUseCase<String, AtivoResponse> {
}
