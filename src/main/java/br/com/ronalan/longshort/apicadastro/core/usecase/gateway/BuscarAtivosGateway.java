package br.com.ronalan.longshort.apicadastro.core.usecase.gateway;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;

public interface BuscarAtivosGateway {

	PaginadoAtivoResponse buscarAtivos(Integer pagina, Integer qtdPorPagina);

}
