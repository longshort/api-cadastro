package br.com.ronalan.longshort.apicadastro.core.usecase.gateway;

import br.com.ronalan.longshort.apicadastro.core.entity.Ativo;
import br.com.ronalan.longshort.apicadastro.core.entity.CorrelacaoAtivo;

public interface BuscarCorrelacaoAtivoGateway {
	CorrelacaoAtivo buscar(Ativo ativo1, Ativo ativo2);
}
