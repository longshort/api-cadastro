package br.com.ronalan.longshort.apicadastro.core.usecase.gateway;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoRequest;

/**
 * Gera evento quando um ativo for cadastrado
 * @author ronaldo.lanhellas
 * */
public interface GerarEventoCadastroAtivoGateway {

	void gerarEvento(AtivoRequest ativo);

}
