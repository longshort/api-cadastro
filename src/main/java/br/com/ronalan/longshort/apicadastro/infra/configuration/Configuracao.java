package br.com.ronalan.longshort.apicadastro.infra.configuration;

import org.springframework.context.annotation.Configuration;

import br.com.ronalan.longshort.apicadastro.core.usecase.gateway.ConfiguracaoGateway;

@Configuration
public class Configuracao implements ConfiguracaoGateway {

	@Override
	public Integer getQuantidadeRegistrosPorPagina() {
		return 5;
	}

}
