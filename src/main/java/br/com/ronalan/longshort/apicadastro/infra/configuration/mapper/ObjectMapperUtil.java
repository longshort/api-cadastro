package br.com.ronalan.longshort.apicadastro.infra.configuration.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public interface ObjectMapperUtil {
	
	static ObjectMapper mapper = new ObjectMapper();

	/**
	 * Converte de uma origem para um destino
	 * 
	 * @param value
	 * @param clazz
	 * @author ronaldo.lanhellas
	 */
	static <T> T convertTo(Object value, Class<T> clazz) {
		mapper.registerModule(new JavaTimeModule());
		return mapper.convertValue(value, clazz);
	}

}
