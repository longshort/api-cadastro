package br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa;

import org.springframework.stereotype.Repository;

import br.com.ronalan.longshort.apicadastro.core.entity.Ativo;
import br.com.ronalan.longshort.apicadastro.core.entity.CorrelacaoAtivo;
import br.com.ronalan.longshort.apicadastro.core.usecase.gateway.BuscarCorrelacaoAtivoGateway;
import br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.mapper.JpaAtivoMapper;
import br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.mapper.JpaCorrelacaoAtivoMapper;
import br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.repository.JpaCorrelacaoAtivoRepository;
import br.com.ronalan.longshort.base.gateway.SalvarGateway;

@Repository
public class CorrelacaoAtivoDataProvider implements SalvarGateway<CorrelacaoAtivo>, BuscarCorrelacaoAtivoGateway {

	private final JpaCorrelacaoAtivoRepository repository;

	public CorrelacaoAtivoDataProvider(JpaCorrelacaoAtivoRepository repository) {
		this.repository = repository;
	}

	@Override
	public CorrelacaoAtivo salvar(CorrelacaoAtivo correlacaoAtivo) {
		repository.save(JpaCorrelacaoAtivoMapper.toJpa(correlacaoAtivo));
		return correlacaoAtivo;
	}

	@Override
	public CorrelacaoAtivo buscar(Ativo ativo1, Ativo ativo2) {
		return JpaCorrelacaoAtivoMapper
				.toModel(repository.findByAtivo1AndAtivo2(JpaAtivoMapper.toJpa(ativo1), JpaAtivoMapper.toJpa(ativo2)));
	}

}
