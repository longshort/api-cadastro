package br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "correlacao_ativo")
public class JpaCorrelacaoAtivoEntity extends JpaBaseEntity {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_ativo1", referencedColumnName = "id", nullable = false)
	private JpaAtivoEntity ativo1;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_ativo2", referencedColumnName = "id", nullable = false)
	private JpaAtivoEntity ativo2;

	public JpaAtivoEntity getAtivo1() {
		return ativo1;
	}

	public void setAtivo1(JpaAtivoEntity ativo1) {
		this.ativo1 = ativo1;
	}

	public JpaAtivoEntity getAtivo2() {
		return ativo2;
	}

	public void setAtivo2(JpaAtivoEntity ativo2) {
		this.ativo2 = ativo2;
	}

}
