package br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;

public interface JpaAtivoRepository extends JpaRepository<JpaAtivoEntity, String> {
   
	List<JpaAtivoEntity> findByCodigo(String codigo);
	
}
