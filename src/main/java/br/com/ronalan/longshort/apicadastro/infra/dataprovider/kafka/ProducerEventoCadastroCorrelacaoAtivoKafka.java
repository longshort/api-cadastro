package br.com.ronalan.longshort.apicadastro.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.CorrelacaoAtivoRequest;
import br.com.ronalan.longshort.apicadastro.core.usecase.gateway.GerarEventoCorrelacaoAtivoGateway;

@Component
@Profile("prod")
public class ProducerEventoCadastroCorrelacaoAtivoKafka implements GerarEventoCorrelacaoAtivoGateway {

	@Value(value = "${app.kafka.cadastro-correlacao-ativos.topic-name}")
	private String kafkaTopicName;

	private final KafkaTemplate<String, CorrelacaoAtivoRequest> kafkaTemplate;

	public ProducerEventoCadastroCorrelacaoAtivoKafka(KafkaTemplate<String, CorrelacaoAtivoRequest> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerarEvento(CorrelacaoAtivoRequest correlacaoAtivo) {
		kafkaTemplate.send(kafkaTopicName, correlacaoAtivo);
	}

}
