package br.com.ronalan.longshort.apicadastro.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.repository.JpaAtivoRepository;
import br.com.ronalan.longshort.apicadastro.template.AtivoRequestTemplateLoader;
import br.com.ronalan.longshort.base.dto.response.ListaErroEnum;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AtualizarAtivoUseCaseIT {

	@Autowired
	private AtualizarAtivoUseCase atualizarUseCase;
	
	@Autowired
	private CadastrarAtivoUseCase cadastrarUseCase;

	@Autowired
	private JpaAtivoRepository repository;

	@Before
	public void before() {
		repository.deleteAll();
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.ronalan.longshort.apicadastro.template");
	}

	@Test
	public void deve_atualizar() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);

		Assert.assertEquals(0, repository.count());
		AtivoResponse responseCadastrar = cadastrarUseCase.executar(request);
		Assert.assertEquals(1, repository.count());
		
		AtivoRequest requestAtualizacao = new AtivoRequest();
		requestAtualizacao.setCodigo(responseCadastrar.getCodigo());
		requestAtualizacao.setDescricao("ATUALIZEI A DESCRICAO");
		AtivoResponse responseAtualizar = atualizarUseCase.executar(requestAtualizacao);
		
		Assert.assertEquals(1, repository.count());
		Assert.assertNotNull(responseAtualizar.getId());
		Assert.assertEquals(requestAtualizacao.getCodigo(), responseAtualizar.getCodigo());
		Assert.assertEquals(requestAtualizacao.getDescricao(), responseAtualizar.getDescricao());
		Assert.assertNotEquals(request.getDescricao(), responseAtualizar.getDescricao());
		
		JpaAtivoEntity jpaAtivoEntity = repository.findAll().get(0);
		Assert.assertEquals(requestAtualizacao.getDescricao(), jpaAtivoEntity.getDescricao());
	}
	

	@Test
	public void nao_deve_atualizar_ativo_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		AtivoResponse response = atualizarUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());

		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(2, response.getResponse().getErros().size());

		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(1).getTipo());
	}
	

	@Test
	public void nao_deve_atualizar_ativo_inexistente() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.RANDOM);
		AtivoResponse response = atualizarUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());
	}

}
