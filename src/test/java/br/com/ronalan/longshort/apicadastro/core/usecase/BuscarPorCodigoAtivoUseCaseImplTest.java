package br.com.ronalan.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.ronalan.longshort.apicadastro.core.entity.Ativo;
import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.ronalan.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.ronalan.longshort.apicadastro.template.AtivoTemplateLoader;
import br.com.ronalan.longshort.base.dto.response.ListaErroEnum;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class BuscarPorCodigoAtivoUseCaseImplTest {

	@InjectMocks
	private BuscarPorCodigoAtivoUseCaseImpl usecase;

	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.ronalan.longshort.apicadastro.template");
	}

	@Test
	public void deve_buscar_ativo_por_codigo() {
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo()))
				.thenReturn(Optional.of(ativoPetr4));

		AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());

		Assert.assertNotNull(response);
		Assert.assertEquals(ativoPetr4.getCodigo(), response.getCodigo());
		Assert.assertEquals(ativoPetr4.getId(), response.getId());
		Assert.assertEquals(ativoPetr4.getDescricao(), response.getDescricao());
		Assert.assertEquals(0, response.getResponse().getErros().size());
	}

	@Test
	public void nao_deve_buscar_ativo_com_codigo_nao_preenchido() {
		AtivoResponse response = usecase.executar("             ");

		Assert.assertNotNull(response);
		Assert.assertEquals("             ", response.getCodigo());
		Assert.assertNull(response.getId());
		Assert.assertNull(response.getDescricao());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
	}

	@Test
	public void nao_deve_retornar_ativo_inexistente() {
		String codigo = "PETR4";
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(codigo)).thenReturn(Optional.empty());

		AtivoResponse response = usecase.executar(codigo);

		Assert.assertNotNull(response);
		Assert.assertEquals(codigo, response.getCodigo());
		Assert.assertNull(response.getId());
		Assert.assertNull(response.getDescricao());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());
	}
}
