package br.com.ronalan.longshort.apicadastro.core.usecase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.ronalan.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;
import br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.ronalan.longshort.apicadastro.infra.dataprovider.jpa.repository.JpaAtivoRepository;
import br.com.ronalan.longshort.apicadastro.template.AtivoRequestTemplateLoader;
import br.com.ronalan.longshort.base.dto.response.ListaErroEnum;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CadastrarAtivoUseCaseIT {

	@Autowired
	private CadastrarAtivoUseCase cadastrarUseCase;

	@Autowired
	private JpaAtivoRepository repository;
	
	@SpyBean
	private GerarEventoCadastroAtivoGateway gerarEventoGateway;

	@Before
	public void before() {
		repository.deleteAll();
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.ronalan.longshort.apicadastro.template");
	}

	@Test
	public void deve_salvar() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.RANDOM);

		Assert.assertEquals(0, repository.count());
		AtivoResponse response = cadastrarUseCase.executar(request);
		Assert.assertEquals(1, repository.count());
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		
		JpaAtivoEntity jpaAtivoEntity = repository.findAll().get(0);
		Assert.assertEquals(request.getCodigo(), jpaAtivoEntity.getCodigo());
		Assert.assertEquals(request.getDescricao(), jpaAtivoEntity.getDescricao());
		Assert.assertNotNull(jpaAtivoEntity.getDataHoraCriacao());
		Assert.assertNotNull(jpaAtivoEntity.getId());
		Mockito.verify(gerarEventoGateway).gerarEvento(Mockito.any());
	}
	

	@Test
	public void nao_deve_salvar_ativo_duplicado() {
		AtivoRequest request = Fixture.from(AtivoRequest.class)
				.gimme(AtivoRequestTemplateLoader.PETR4);
		
		cadastrarUseCase.executar(request);
		Assert.assertEquals(1, repository.count());
		
		AtivoResponse response = cadastrarUseCase.executar(request);

		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());

		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.DUPLICIDADE, response.getResponse().getErros().get(0).getTipo());

		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		
		Assert.assertEquals(1, repository.count());
		Mockito.verify(gerarEventoGateway, Mockito.times(1)).gerarEvento(Mockito.any());
	}
	

	@Test
	public void nao_deve_salvar_ativo_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("          ");
		AtivoResponse response = cadastrarUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());

		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(2, response.getResponse().getErros().size());
		
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(1).getTipo());
		Assert.assertEquals(0, repository.count());
		Mockito.verify(gerarEventoGateway, Mockito.times(0)).gerarEvento(Mockito.any());
	}

}
