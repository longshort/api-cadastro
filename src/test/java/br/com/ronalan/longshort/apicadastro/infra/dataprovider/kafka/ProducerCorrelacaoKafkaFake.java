package br.com.ronalan.longshort.apicadastro.infra.dataprovider.kafka;

import org.springframework.stereotype.Component;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.CorrelacaoAtivoRequest;
import br.com.ronalan.longshort.apicadastro.core.usecase.gateway.GerarEventoCorrelacaoAtivoGateway;

@Component
public class ProducerCorrelacaoKafkaFake implements GerarEventoCorrelacaoAtivoGateway {

	@Override
	public void gerarEvento(CorrelacaoAtivoRequest ativo) {
		// TODO Auto-generated method stub
	}

}
