package br.com.ronalan.longshort.apicadastro.infra.dataprovider.kafka;

import org.springframework.stereotype.Component;

import br.com.ronalan.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.ronalan.longshort.apicadastro.core.usecase.gateway.GerarEventoCadastroAtivoGateway;

@Component
public class ProducerKafkaFake implements GerarEventoCadastroAtivoGateway {

	@Override
	public void gerarEvento(AtivoRequest ativo) {
		// TODO Auto-generated method stub
	}

}
